using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MostrarRanking : MonoBehaviour
{

    public void Mostrar()
    {

        RankingManager rm = GetComponent<RankingManager>();

        rm.MostrarRanking();
    }
    // Start is called before the first frame update
    void Start()
    {
        Mostrar();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
