using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;
using System.Security.Cryptography;
using Newtonsoft.Json.Linq;

public class SaveAllData : MonoBehaviour
{

    public RespawnEnemies thisAllEnemigos;
    public Transform thisPlayerPosition;


    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;

        if (ManagerPlayer.Instance.CONTINUE) {

            LoadGame();
            ManagerPlayer.Instance.CONTINUE = false;
        }
    }

 

    public class DataSave
    {

        public int levelEnemigos;
        public Vector3 playerPosition;
        public float score;
        public float life;

    }

    public DataSave data;


    public void SaveGame()
    {
        string saveFIlePath = Application.persistentDataPath + "/JsavegameE.sav";

        // using StreamWriter sw = new StreamWriter(saveFIlePath); 

        DataSave dataS = new DataSave();

        dataS.levelEnemigos = thisAllEnemigos.levelDifficulty;
        dataS.playerPosition = thisPlayerPosition.position;
        dataS.score = ManagerPlayer.Instance.SCORE;
        dataS.life = ManagerPlayer.Instance.LIFE;

        // sw.WriteLine(JsonConvert.SerializeObject(dataS));

        byte[] encryptedMessage = Encrypt(JsonConvert.SerializeObject(dataS));
        File.WriteAllBytes(saveFIlePath, encryptedMessage);
        Debug.Log("Guardado en " + saveFIlePath);


    }

    public void LoadGame()
    {
        string saveFIlePath = Application.persistentDataPath + "/JsavegameE.sav";

        byte[] Message; // = File.ReadAllBytes(saveFIlePath);
        
        //StreamReader sr = StreamReader.Null;

        try
        {
            Message = File.ReadAllBytes(saveFIlePath);

        }
        catch (FileNotFoundException noFile)
        {
            Debug.LogError("PARTIDA NO ENCONTRADA - SE CREA UNA NUEVA");
            SaveGame();

            Message = File.ReadAllBytes(saveFIlePath);

        }

       // string dataR = sr.ReadToEnd();
        string decryptedMessage = Decrypt(Message);
        data = JsonConvert.DeserializeObject<DataSave>(decryptedMessage);

        thisAllEnemigos.levelDifficulty = data.levelEnemigos;
        thisPlayerPosition.position = thisPlayerPosition.position;
        ManagerPlayer.Instance.SCORE = data.score;
        ManagerPlayer.Instance.LIFE = data.life;
        UpdateInterface.Instance.UpdateScoreAndLife();
       //sr.Close();
        Debug.Log("Cargado en " + saveFIlePath);

    }



    byte[] _key = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };
    byte[] _inicializationVector = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };

    byte[] Encrypt(string message)
    {
        AesManaged aes = new AesManaged();
        ICryptoTransform encryptor = aes.CreateEncryptor(_key, _inicializationVector);

        MemoryStream memoryStream = new MemoryStream();
        CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
        StreamWriter streamWriter = new StreamWriter(cryptoStream);

        streamWriter.WriteLine(message);

        streamWriter.Close();
        cryptoStream.Close();
        memoryStream.Close();

        return memoryStream.ToArray();
    }

    string Decrypt(byte[] message)
    {
        AesManaged aes = new AesManaged();
        ICryptoTransform decrypter = aes.CreateDecryptor(_key, _inicializationVector);

        MemoryStream memoryStream = new MemoryStream(message);
        CryptoStream cryptoStream = new CryptoStream(memoryStream, decrypter, CryptoStreamMode.Read);
        StreamReader streamReader = new StreamReader(cryptoStream);

        string decryptedMessage = streamReader.ReadToEnd();

        memoryStream.Close();
        cryptoStream.Close();
        streamReader.Close();

        return decryptedMessage;
    }



}
