using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[AddComponentMenu("Ponce/Manager/Escenas")]
public class ChangeScenes : MonoBehaviour
{
    public void toMain() {

        SceneManager.LoadScene("Intro");

    }

    public void toGame()
    {
        ManagerPlayer.Instance.SCORE = 0;
        ManagerPlayer.Instance.LIFE = 100;
        SceneManager.LoadScene("Game");

    }

    public void toGameOver()
    {
        SceneManager.LoadScene("GameOver");

    }

    public void toRanking()
    {
        SceneManager.LoadScene("RankingTop");

    }

    public void toExit()
    {
        Application.Quit();
    }

    public void continueYES() {

        ManagerPlayer.Instance.CONTINUE = true;
    }
}
