using System.Collections;
using System.Collections.Generic;
using UnityEngine;


///<summary>
///Clase simple donde se crea un metodo para cada direccion que ha de seguir el propio enemigo, y la corrutine que le da movimiento, 
///cuando acaba la corrutina se deshabilita.
///</summary>

[AddComponentMenu("Ponce/Enemigos/Run")]
public class RunToDirection : MonoBehaviour
{
    public float speed;
    public float time;

    public void MoveDown() {

        StartCoroutine(Move(new Vector3(0,-400,0)));
    }


    public void MoveUp()
    {

        StartCoroutine(Move(new Vector3(0, 400, 0)));
    }

    public void MoveLeft()
    {

        StartCoroutine(Move(new Vector3(-400, 0, 0)));
    }

    public void MoveRight()
    {

        StartCoroutine(Move(new Vector3(400, 0, 0)));
    }

    Vector3 fromPos;
    Vector3 endPos;

    IEnumerator Move(Vector3 direction)
    {

        //float inTime = 5f;

        fromPos = transform.position;
        endPos = direction;

        for (float t = 0f; t <= 1; t += Time.deltaTime / speed)
        {

            transform.position = Vector3.Lerp(fromPos, endPos, t);

            yield return null;
        }

        transform.rotation = Quaternion.Euler(Vector3.zero);

        gameObject.SetActive(false);


    }

}
