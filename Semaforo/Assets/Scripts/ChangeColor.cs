using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


///<summary>
///En esta clase sirve para cambiar el color cada vez que el jugador le da al boton espacio. 
///</summary>

[AddComponentMenu("Ponce/Enemigos/Cambiar Color")]


public class ChangeColor : MonoBehaviour
{
    /*
     * Los colores es un array publico para que sea facil implementar mas colores si hace falta.
     * 
     */

    public Color[] colorsPlayer;
    private TagColor colortag;
    int positionColor;
    Renderer thisMR;

    // Start is called before the first frame update
    void Start()
    {
        thisMR = gameObject.GetComponent<MeshRenderer>();
        colortag = gameObject.GetComponent<TagColor>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump") | Input.GetButtonDown("Fire1") | Input.GetButtonDown("Submit")) {
            positionColor++;

            if (positionColor >= colorsPlayer.Length)
                positionColor = 0;

            thisMR.material.SetColor("_Color",colorsPlayer[positionColor]);

            switch (positionColor)
            {
                case 0:
                    colortag.color = TagColor.colorsTag.red;
                    break;
                case 1:
                    colortag.color = TagColor.colorsTag.yellow;
                    break;
                case 2:
                    colortag.color = TagColor.colorsTag.green;
                    break;
                default:
                    break;
            }

        }
    }
}
