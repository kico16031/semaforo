using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Ponce/Enemigos/Detector")]



public class DetectorCollisionPlayer : MonoBehaviour
{
    TagColor mycolorTag;

    

    private void Start()
    {
        mycolorTag = GetComponent<TagColor>();

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (other.gameObject.GetComponent<TagColor>().color == mycolorTag.color)
            {
                ManagerPlayer.Instance.SCORE += 50;
                UpdateInterface.Instance.UpdateScoreAndLife();
                UpdateInterface.Instance.playScoreAdd();
            }
            else
            {
                ManagerPlayer.Instance.LIFE -= 10;
                CameraShake.Instance.ShakeCamera(20f, 0.5f);
                UpdateInterface.Instance.UpdateScoreAndLife();
                UpdateInterface.Instance.playQuitLife();
            }
        }
    }

}
