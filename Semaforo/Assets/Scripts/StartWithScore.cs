using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[AddComponentMenu("Ponce/Interfaz/StartScore")]
public class StartWithScore : MonoBehaviour
{

    private TextMeshProUGUI ScoreText;
    // Start is called before the first frame update
    void Start()
    {
        ScoreText = GetComponent<TextMeshProUGUI>();

        ScoreText.text = "SCORE: " + ManagerPlayer.Instance.SCORE;

        if(ManagerPlayer.Instance.SCORE > ManagerPlayer.Instance.MAXSCORE)
        {
            //PlayerPrefs.SetFloat("Player Score", ManagerPlayer.Instance.SCORE);
            SaveScore.Instance.SaveScoreMax();

            ManagerPlayer.Instance.MAXSCORE = ManagerPlayer.Instance.SCORE;
            ScoreText.text = "MAX SCORE!!: " + ManagerPlayer.Instance.SCORE;
            ScoreText.fontSize = 150;

        }


    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
