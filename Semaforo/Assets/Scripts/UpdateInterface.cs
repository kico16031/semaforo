using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;


///<summary>
///Clase para actualizar la interfaz del juego
///</summary>

[AddComponentMenu("Ponce/Interfaz/Actualizar Interfaz")]

public class UpdateInterface : MonoBehaviour
{
     private static UpdateInterface _instance;

    public TextMeshProUGUI ScoreText;
    public TextMeshProUGUI LifeText;

    private Animator animLife;
    private Animator animScore;


    public static UpdateInterface Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            animLife = LifeText.gameObject.GetComponent<Animator>();
            animScore = ScoreText.gameObject.GetComponent<Animator>();

        }
        else
        {
            Destroy(this);
        }
    }

    public void UpdateScoreAndLife() {

        ScoreText.text = "SCORE: " +  ManagerPlayer.Instance.SCORE;
        LifeText.text = "LIFE: " + ManagerPlayer.Instance.LIFE;

        if (ManagerPlayer.Instance.LIFE <= 0) {
            ManagerPlayer.Instance.CONTINUE = false;
            SceneManager.LoadScene("GameOver");
        }
    }

    public void playQuitLife() 
    {
        animLife.SetTrigger("QuitLife");
    }

    public void playScoreAdd()
    {
        animScore.SetTrigger("AddScore");
    }


}
