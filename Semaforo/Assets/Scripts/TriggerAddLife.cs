using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Ponce/Utilidades/AddLife")]

public class TriggerAddLife : MonoBehaviour
{
    private void Start()
    {
        Destroy(gameObject,5);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player") {

            ManagerPlayer.Instance.LIFE += 10;
            UpdateInterface.Instance.UpdateScoreAndLife();

            Destroy(gameObject);

        }
    }
}
