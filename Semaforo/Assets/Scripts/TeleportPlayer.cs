using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Ponce/Utilidades/TeleportPlayer")]
public class TeleportPlayer : MonoBehaviour
{
    public Vector3 newPosition;
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player") {

            other.gameObject.transform.position = newPosition;


        }
    }
}
