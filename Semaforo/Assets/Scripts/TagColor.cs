using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Ponce/Jugador/ColorTag")]
public class TagColor : MonoBehaviour
{

    public enum colorsTag{
        red = 0, yellow = 1, green = 2,

}

    public colorsTag color;
}
