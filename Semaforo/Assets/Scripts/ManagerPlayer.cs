using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


[AddComponentMenu("Ponce/Manager/ManagerPlayer")]
public class ManagerPlayer : MonoBehaviour
{
    public float SCORE;
    public float MAXSCORE;
    public float LIFE;
    public bool CONTINUE = false;

    private static ManagerPlayer _instance;

    public static ManagerPlayer Instance
    {
        get { return _instance; }

    }

    void Awake()
    {

        if (_instance == null)
        {

            _instance = this;
            DontDestroyOnLoad(this.gameObject);

        }
        else
        {
            Destroy(this);
        }
    }

}
