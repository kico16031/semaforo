﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary>
///Clase muy simple pensada para los enemigos, para que roten en una direccion aleatoria
///</summary>

[AddComponentMenu("Ponce/Enemigos/Rotate")]
public class Rotate : MonoBehaviour
{
    public float velocity;
    public bool inverseDirection;

    // Start is called before the first frame update
    void Start()
    {
        inverseDirection = Random.value < 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        if(inverseDirection)
            transform.Rotate(Vector3.forward * velocity/100);
        else
            transform.Rotate(Vector3.back * velocity / 100);
    }
}
