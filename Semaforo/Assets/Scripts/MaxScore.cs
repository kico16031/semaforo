using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MaxScore : MonoBehaviour
{
    public TextMeshProUGUI textMesh;
    // Start is called before the first frame update
    void Start()
    {
        //ManagerPlayer.Instance.MAXSCORE = PlayerPrefs.GetFloat("Player Score");

        Cursor.lockState = CursorLockMode.Locked;

        SaveScore.Instance.LoadScoreMax();


        ManagerPlayer.Instance.MAXSCORE = SaveScore.Instance.data.score;

        textMesh.text = "SCORE MAX : " + ManagerPlayer.Instance.MAXSCORE;

    }

    
}
