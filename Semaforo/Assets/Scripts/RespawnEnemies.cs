using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary>
///Clase para hacer respawn
///</summary>

[AddComponentMenu("Ponce/Enemigos/Respawn")]

public class RespawnEnemies : MonoBehaviour
{

    public Vector3 upPosition;
    public Vector3 downPosition;
    public Vector3 rightPosition;
    public Vector3 leftPosition;

    public int levelDifficulty = 0;

    public float[] ScoreDifficulty;
    public float[] timeInDifficulty;


    private GameObject[] enemies;// = new GameObject[5];

    public float timeRespawn;

    void Start()
    {
        /*
         * Recorro todos los hijos del gameobjet que contiene a los propios enemigos, filtro por tag por si acaso se cuelo un gameobject no deseado, 
         * los recorro para anyadirlos en el array, y luego sacarlos mas adelante.
         * 
         */
       
        enemies = GameObject.FindGameObjectsWithTag("Enemy");

        foreach (GameObject item in enemies)
        {
            item.SetActive(false);
        }

        /*
         * 
         *Nada mas empezar el respawn empieza a funcionar
         *
         */

        //levelDifficulty = 0;

        StartCoroutine(Respawn());

    }

 
    IEnumerator Respawn()
    {
        /*
         * 
         *Genero el numRandom para elegir aleatoriamente la direccion del enemigo
         *
         *el num random enemy es para elegir aleatoriamente el enemigo del array
         *
         */
        int numRandom = Random.Range(0, 3);
        //int numRandomEnemy = Random.Range(0, enemies.Length);
        GameObject enemy = GetEnemy();



        /*
         *
         *Hago el switch para que segun el caso de la direccion los genera en distinta posicion y les da la direccion a seguir
         * 
         */

        switch (numRandom)
        {
            case 0:
                enemy.SetActive(true);
                enemy.transform.position = upPosition;
                enemy.transform.rotation = Quaternion.Euler(0, 0, 90);
                enemy.GetComponent<RunToDirection>().MoveDown();
                break;
            case 1:
                enemy.SetActive(true);
                enemy.transform.position = downPosition;
                enemy.transform.rotation = Quaternion.Euler(0, 0, 90);
                enemy.GetComponent<RunToDirection>().MoveUp();
                break;
            case 2:
                enemy.SetActive(true);
                enemy.transform.position = leftPosition;
                enemy.transform.rotation = Quaternion.Euler(0, 0, 0);
                enemy.GetComponent<RunToDirection>().MoveRight();
                break;
            case 3:
                enemy.SetActive(true);
                enemy.transform.position = rightPosition;
                enemy.transform.rotation = Quaternion.Euler(0, 0, 0);
                enemy.GetComponent<RunToDirection>().MoveLeft();
                break;
            default:
                //Nunca deberia pasar esto
                Debug.LogError("Enemy respawn in unknown position");
                break;
        }

        yield return new WaitForSeconds(timeRespawn);
        /*
         *
         *Cuando acaba el tiempo de espera se vuelve a llamar a la misma corrutina para que nunca termine
         * 
         */

        while (ManagerPlayer.Instance.SCORE > ScoreDifficulty[levelDifficulty] && (levelDifficulty < ScoreDifficulty.Length)) {

            timeRespawn = timeInDifficulty[levelDifficulty];
            
            levelDifficulty++;


        }
       

        yield return StartCoroutine(Respawn());

    }

    /*
     *Este metodo existe para no coger nunca un enemigo que esta en uso, si esta activo significa que esta en la escena ejecutandose 
     *y por lo tanto se llama recursivamente asi mismo para encontrar otro, sin perder nunca el valor de la aletoriedad 
     * 
     */
    private GameObject GetEnemy()
    {
        int numRandomEnemy = Random.Range(0, enemies.Length);
        GameObject enemy = enemies[numRandomEnemy];
        if (!enemy.activeSelf)
        {
            return enemy;
        }
        else {
            return GetEnemy();
        }

        
    }
}
