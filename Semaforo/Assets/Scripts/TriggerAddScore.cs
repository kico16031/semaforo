using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Ponce/Utilidades/AddScore")]

public class TriggerAddScore : MonoBehaviour
{
    private void Start()
    {
        Destroy(gameObject,5);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player") {


            ManagerPlayer.Instance.SCORE += 15;
            UpdateInterface.Instance.UpdateScoreAndLife();
            UpdateInterface.Instance.playScoreAdd();

            Destroy(gameObject);

        }
    }
}
