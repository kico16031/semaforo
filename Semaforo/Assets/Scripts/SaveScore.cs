using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SaveScore : MonoBehaviour
{


    private static SaveScore _instance;

    public static SaveScore Instance
    {
        get { return _instance; }

    }

    void Awake()
    {

        if (_instance == null)
        {

            _instance = this;
            DontDestroyOnLoad(this.gameObject);

        }
        else
        {
            Destroy(this);
        }
    }

    public class DataSaveScore {

        public float score;
    
    }

    public DataSaveScore data;


    public void SaveScoreMax() {
        string saveFIlePath = Application.persistentDataPath + "/JsavegameScore.sav";

        using StreamWriter sw = new StreamWriter(saveFIlePath);

        DataSaveScore dataS = new DataSaveScore();
        dataS.score = ManagerPlayer.Instance.SCORE;

        sw.WriteLine(JsonConvert.SerializeObject(dataS));
        
        Debug.Log("Guardado en " + saveFIlePath);


    }

    public void LoadScoreMax()
    {
        string saveFIlePath = Application.persistentDataPath + "/JsavegameScore.sav";
        StreamReader sr = StreamReader.Null;

        try
        {
            sr = new StreamReader(saveFIlePath);

        }
        catch (FileNotFoundException noFile)
        {
            Debug.LogError("PARTIDA NO ENCONTRADA - SE CREA UNA NUEVA");
            SaveScoreMax();

            sr = new StreamReader(saveFIlePath);

        }

        string dataR = sr.ReadToEnd();
        data = JsonConvert.DeserializeObject<DataSaveScore>(dataR);

        //caso extranyo que me ha pasado de que carga bien pero la partida estaba corrupta y no le daba valor al objeto
        if (data == null) {
            SaveScoreMax();
        }

        sr.Close();
        Debug.Log("Cargado en " + saveFIlePath);

    }


}
