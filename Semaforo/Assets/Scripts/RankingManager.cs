﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/////db
using System.Data;
using System.IO;
using Mono.Data.Sqlite;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class RankingManager : MonoBehaviour
{

    string rutaDB;
    string strConexion;
    string DBfile = "RankingDB.sqlite";
    public TMPro.TextMeshProUGUI pos;

    //conectar db
    IDbConnection dbConnection;
    //ejecutar comandos
    IDbCommand dbComand;
    //leer db
    IDataReader reader;
    // Start is called before the first frame update

    public List<string> rankings = new List<string>();


    void AbrirDB() {

        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor)
        {
            rutaDB = Application.dataPath + "/StreamingAssets/" + DBfile;
        }
        else if (Application.platform == RuntimePlatform.Android)
        {

            rutaDB = Application.persistentDataPath + "/" + DBfile;

            if (!File.Exists(rutaDB))
            {
                WWW loadDB = new WWW("jar;file://" + Application.dataPath + DBfile);

                while (!loadDB.isDone)
                {

                }
                File.WriteAllBytes(rutaDB, loadDB.bytes);
            }

        }

        // crear y abrir conexion
        rutaDB = Application.dataPath + "/StreamingAssets/" + DBfile;
        strConexion = "URI=file:" + rutaDB;
        dbConnection = new SqliteConnection(strConexion);

        dbConnection.Open();
        //CreateTable();
    }

    private void ObtenerRanking()
    {

        AbrirDB();

        dbComand = dbConnection.CreateCommand();

        string sqlQuery = "select * from Ranking order by score DESC limit 10";

        dbComand.CommandText = sqlQuery;

        reader = dbComand.ExecuteReader();


        int index = 1;

        while (reader.Read())
        {
            //obtener datos

            string name = reader["Name"].ToString();

            int score = Convert.ToInt32(reader["Score"]);
             
            pos.text = pos.text + "\n" + "#" + (index++).ToString("D2")+ "             " + name + "            " + score.ToString("D8");

        }

        reader.Close();
        reader = null;

    }

    public void InsertScore(string name, int score) {

        AbrirDB();
        
        dbComand = dbConnection.CreateCommand();

        string sqlQuery = "INSERT INTO Ranking (Name, Score) values( \"" + name + "\", \"" + score + "\")";

        dbComand.CommandText = sqlQuery;
        dbComand.ExecuteScalar();

        CerrarDB();

    }



    public void MostrarRanking() {

        ObtenerRanking();

        CerrarDB();


    }


    void CerrarDB()
{

    //cerrar

    dbComand.Dispose();
    dbComand = null;
    dbConnection.Close();
    dbConnection = null;
}






    //private void CreateTable()
    //{
    //    //using (dbConnection = new SqliteConnection(conn))
    //    //{
    //    //    dbConnection.Open();
    //        dbComand = dbConnection.CreateCommand();
    //        string sqlQuery = "CREATE TABLE IF NOT EXISTS [Ranking] (" +
    //            "[id] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT," +
    //            "[Name] VARCHAR(255)  NOT NULL," +
    //            "[Score] INTEGER NOT NULL)";
    //        dbComand.CommandText = sqlQuery;
    //        dbComand.ExecuteScalar();
    //       // dbconn.Close();
    //   // }
    //}



    //void DeleteScore(int id)
    //{

    //    AbrirDB();

    //    dbComand = dbConnection.CreateCommand();

    //    string sqlQuery = "DELETE FROM Ranking WHERE PlayerID = \"" + id + "\"";

    //    dbComand.CommandText = sqlQuery;
    //    dbComand.ExecuteScalar();

    //    //CerrarDB();

    //}

}

