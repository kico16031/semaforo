﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Ponce/Prototype/SpawnerCubos")]

public class CubeSpawner : MonoBehaviour
{
    public float timeRespawnLife;
    public float timeRespawnScore;

    private float randomX;
    private float randomY;

    private void Start()
    {
        StartCoroutine(RespawnLife());
        StartCoroutine(RespawnScore());
    }

  

    IEnumerator RespawnLife()
    {
        randomX = Random.Range(-250, 250);
        randomY = Random.Range(-150, 100);
        GameObject cubeLife = ProcCube.Clone(new Vector3(randomX,randomY,-1));
        MeshRenderer rend = cubeLife.GetComponent<MeshRenderer>();
        rend.material.color = new Color(0, 190, 255);
        cubeLife.AddComponent<TriggerAddLife>();

        cubeLife.transform.SetParent(gameObject.transform);

        yield return new WaitForSeconds(timeRespawnLife);

        /*
         *
         *Cuando acaba el tiempo de espera se vuelve a llamar a la misma corrutina para que nunca termine
         * 
         */

        StartCoroutine(RespawnLife());
    }

    IEnumerator RespawnScore()
    {
        randomX = Random.Range(-250, 250);
        randomY = Random.Range(-150, 100);

        GameObject cubeLife = ProcCube.Clone(new Vector3(randomX, randomY, -1));


        MeshRenderer rend = cubeLife.GetComponent<MeshRenderer>();
        rend.material.color = new Color(233, 0, 255);
        cubeLife.AddComponent<TriggerAddScore>();

        cubeLife.transform.SetParent(gameObject.transform);

        yield return new WaitForSeconds(timeRespawnScore);

        /*
         *
         *Cuando acaba el tiempo de espera se vuelve a llamar a la misma corrutina para que nunca termine
         * 
         */

        StartCoroutine(RespawnScore());
    }
}
