using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

///<summary>
///Clase simple de movimiento del jugador
///</summary>
[AddComponentMenu("Ponce/Jugador/Controlador")]

public class PlayerController : MonoBehaviour
{
    public float speed = 10.0f;
    public GameObject panelPause;
    public Button focus;

    void Update()
    {
        float translationH = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        float translationV = Input.GetAxis("Vertical") * speed * Time.deltaTime;

        transform.Translate(new Vector3(translationH, translationV, 0));

        //transform.GetComponent<Rigidbody>().AddForce(new Vector3(translationH, translationV, 0));

        if (Input.GetKeyDown(KeyCode.Escape) | Input.GetKeyDown(KeyCode.Joystick1Button6)) {

            if (Time.timeScale == 1)
            {
                PauseOn();
            }
            else {
                PauseOff();

            }
        }
    }

    public void PauseOn() {
        panelPause.SetActive(true);
        Time.timeScale = 0;
        focus.Select();
    }

    public void PauseOff()
    {
        panelPause.SetActive(false);
        Time.timeScale = 1;
    }

}
