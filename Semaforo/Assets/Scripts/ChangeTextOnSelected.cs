﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ChangeTextOnSelected : MonoBehaviour, ISelectHandler, IDeselectHandler
{

    Button thisButton;
    private bool selected;
    public string[] texts;
    public int indexText = 0;
    // Start is called before the first frame update
    void Start()
    {
        thisButton = gameObject.GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        if (selected) {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                indexText++;
                if (indexText > texts.Length - 1)
                {
                    indexText = 0;
                }

                ChangeText(texts[indexText]);
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                indexText--;
                if (indexText < 0)
                {
                    indexText = texts.Length - 1;
                }

                ChangeText(texts[indexText]);
            }

        }
    }
    public void ChangeText(string text) {
        thisButton.GetComponentInChildren<TextMeshProUGUI>().text = text;



    }

    public void OnSelect(BaseEventData eventData)
    {
        selected = true;
    }

    public void OnDeselect(BaseEventData eventData)
    {
        selected = false;
    }

    //public void OnSelect(BaseEventData eventData)
    //{
    //    Debug.Log(this.gameObject.name + " was selected");
    //}
}
