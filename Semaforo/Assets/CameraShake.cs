using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

//CameraShake.Instance.ShakeCamera(50, 5);

public class CameraShake : MonoBehaviour
{

    private static CameraShake _instance;

    CinemachineVirtualCamera cinemachineVirtualCamera;

    Vector3 cameraInitialPosition;
    private float startingintensity;
    private float shakimerTotal;
    public float shakeTimer;


    Camera mainCamera;

    CinemachineBasicMultiChannelPerlin cinemachineBasicMultiChannelPerlin;

    public static CameraShake Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            cinemachineVirtualCamera = GetComponent<CinemachineVirtualCamera>();
            cameraInitialPosition = transform.position;
            cinemachineBasicMultiChannelPerlin = cinemachineVirtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        }
        else
        {
            Destroy(this);
        }
    }
    


    public void ShakeCamera(float intensity, float time)
    {
        //CinemachineBasicMultiChannelPerlin cinemachineBasicMultiChannelPerlin = cinemachineVirtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        //cinemachineBasicMultiChannelPerlin.m_AmplitudeGain = intensity;
        startingintensity = intensity;
        shakimerTotal = time; 
        shakeTimer = time;

    }
    private void Update()
    {
        if (shakeTimer > 0)
        {
            shakeTimer -= Time.deltaTime;

            //if (shakeTimer <= 0f)
            //{
            // Timer over!
            cinemachineBasicMultiChannelPerlin.m_AmplitudeGain = Mathf.Lerp(startingintensity, 0f, shakeTimer / shakimerTotal);
            //}
            //else
            //{
            //     //  transform.position = 
            //}


        }
        else {
            cinemachineBasicMultiChannelPerlin.m_AmplitudeGain = 0;
            transform.position = cameraInitialPosition;
            transform.rotation = Quaternion.Euler(Vector3.zero);
        }
    }

    //IEnumerator Fade()
    //{

    //    shakeTimer -= Time.deltaTime;
    //    CinemachineBasicMultiChannelPerlin cinemachineBasicMultiChannelPerlin = cinemachineVirtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
    //    cinemachineBasicMultiChannelPerlin.m_AmplitudeGain =
    //    Mathf.Lerp(startingintensity, 0f, shakeTimer / shakimerTotal);
    //    yield return new WaitForSeconds(0.5f);


    //}
    }