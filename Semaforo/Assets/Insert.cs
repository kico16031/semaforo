using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Insert : MonoBehaviour
{
    public TMPro.TextMeshProUGUI letra1;
    public TMPro.TextMeshProUGUI letra2;
    public TMPro.TextMeshProUGUI letra3;

    int score;

    public GameObject database;

    // Start is called before the first frame update
    void Start()
    {
        score = (int)ManagerPlayer.Instance.SCORE;
    }

    public void insertDatabase() {

        RankingManager rm = database.GetComponent<RankingManager>();

        rm.InsertScore((letra1.text + letra2.text + letra3.text), score);
    }
}
